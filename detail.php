<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>PERPUSTAKAAN | DETAIL BUKU</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<?php
//menggunakan fungsi dari file PHP lain
include "koneksi.php";

if (isset($_GET['id'])) {
	$id_buku=$_GET['id'];
	$sSql="SELECT a.id_buku as xKode, b.judul as xJdl, c.kategori as xKat,
	d.nama as xNmUser, a.tgl_input as xTgl, 
	a.status as xSts FROM t_buku a
	INNER JOIN t_judul b ON a.id_judul = b.id_judul
	INNER JOIN t_kategori c ON b.id_kategori = c.id_kategori
	INNER JOIN t_user d ON a.id_user = d.id_user
	where (a.id_buku = '$id_buku')";
	
	$result=mysqli_query($koneksi, $sSql) or die (mysql_error());
	
	if($data=mysqli_fetch_array($result)){ ?> 
        <table>
            <tr>
                <th>Kode</th>
                 <td>: <?php echo "$data[xKode]"; ?></td>
            </tr>
            <tr>
                <th>Judul</th>
                <td>: <?php echo "$data[xJdl]" ?></td>
            </tr>
             <tr>
                <th>Kategori</th>
                <td>: <?php echo "$data[xKat]"; ?></td>
            </tr>	
             <tr>
                <th>User Input</th>
                <td>: <?php echo "$data[xNmUser]"; ?></td>
            </tr>
             <tr>
                <th>Waktu Input</th>
                <td>: <?php $date = date_create($data['xTgl']);
					 echo date_format($date,'d M Y (H:1:s)'); ?> </td>
            </tr>	
             <tr>
                <th>Status</th>
                <td>: <?php if ($data['xSts'] == '1'){
						echo "Available";
					}
					else {
						echo "Not Available";
						} ?>
                </td>
            </tr>			
           
        </table>
   
	<?php
	}
	else {echo "<h1>Data Buku tidak ditemukan</h1><br />";}
}
	
?>
<a href="./index2.php">Kembali ke Daftar Buku</a>
</body>
</html>
