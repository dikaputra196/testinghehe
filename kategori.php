<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>PERPUSTAKAAN | KATEGORI BUKU</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<?php
//menggunakan fungsi dari file PHP lain
include "koneksi.php";

//query SQL join 4 table
$sSql="SELECT * FROM t_kategori order by 1;";

//jika query error maka akan keluar dari semua fungsi
$result=mysqli_query($koneksi, $sSql) or die(mysqli_error());

//membuat nomor urut
$counter = 1; 

//menghitung jumlah record dari result set
echo "<h1>Jumlah Data: ". mysqli_num_rows($result)."</h1>";

if(mysqli_num_rows($result) > 0){ //jika query menghasilkan record
	//membentuk struktur tabel
	echo "<table border=\"1\"> 
			<tr>
				<th class=\"no\">No</th>
				<th class=\"no\">Kode Kategori</th>
				<th>Nama Kategori</th>
				<th>Status</th>
			</tr>";

	//mengisi record pada tabel
	while($data=mysqli_fetch_array($result))
	{
		$sts="Aktif";
		if ($data["status"]!="1")
			{
				$sts="Non Aktif";
			}
		
	echo "<tr>
			<td class=\"no\">$counter</td>
			<td>$data[id_kategori]</td>
			<td>$data[kategori]</td>
			<td>$sts</td>
		  </tr>";
		$counter++;
	} 
	//menutup struktur dari tabel
	echo "</table>";
}
else {
	//jka query tidak menghasilkan record
	echo "<h2>Data Kategori Tidak Ditemukan</h2>";
	}
?>
</body>
</html>
