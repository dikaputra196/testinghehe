<?php
	include "userlog.php";
	include "../koneksi.php";

	$idUSer = $_REQUEST['id'];

	//Select data
	$query = "select * from t_user where id_user='$idUSer' ";
	$execute = mysqli_query($koneksi,$query) or die(mysqli_error($execute));
	$result = mysqli_fetch_assoc($execute);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Update User</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
	<form name="updateAdmin" action="execute-update.php" method="POST" enctype="multipart/form-data">
		<table align="center">
			<tbody>
				<input type="hidden" name="id" value="<?= $result['id_user'] ?>">
				<tr>
					<th>Nama</th>
					<td>:</td>
					<td>
						<input type="text" name="nama" value="<?= $result['nama'] ?>">
					</td>
				</tr>
				<tr>
					<th>Password</th>
					<td>:</td>
					<td>
						<input type="password" name="password" value="<?= $result['password'] ?>">
					</td>
				</tr>
				<tr>
					<th>Email</th>
					<td>:</td>
					<td>
						<input type="email" name="email" value="<?= $result['email'] ?>">
					</td>
				</tr>
				<tr>
					<th>Hp</th>
					<td>:</td>
					<td>
						<input type="text" name="hp" value="<?= $result['hp'] ?>">
					</td>
				</tr>
				<tr>
					<th>Status</th>
					<td>:</td>
					<td>
						<select name="status">
							<?php
								if($result['status']==1){ ?>
									<option value="<?= $result['status'] ?>">Status sekarang - Aktif</option>
									<option value="0">Tidak Aktif</option>
							<?php }else{ ?>
									<option value="<?= $result['status'] ?>">Status sekarang - Tidak Aktif</option>
									<option value="1">Aktif</option>
							<?php } ?>
							
						</select>
					</td>
				</tr>
				<tr>
					<th>Foto</th>
					<td>:</td>
					<td>
						<img src="../img-user/<?= $result['foto'] ?>" width="60px" height="60px">
					</td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td>
						<input type="file" name="foto">
					</td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td>
						<input type="submit" name="submit">
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</body>
</html>