<?php
include "userlog.php";
include "../koneksi.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Menambah User Baru</title>
<link href="../style.css" type="text/css" rel="stylesheet" />

<script type="text/javascript">
	function cek_form(frm){	
		if(frm.user.value==""){
			alert("Kolom user id masih kosong!");
			frm.user.focus();
			return false;
		}else if(frm.nama.value==""){
			alert("Kolom nama masih kosong!");
			frm.nama.focus();
			return false;
		}else if(frm.pass.value==""){
			alert("Kolom password masih kosong!");
			frm.pass.focus();
			return false;
		}else if(frm.repass.value==""){
			alert("Kolom re-password masih kosong!");
			frm.repass.focus();
			return false;
		}else if(frm.pass.value !== frm.repass.value){
			alert("Kolom re-password tidak cocok!");
			frm.repass.focus();
			return false;
			
		}else return true;
	}
</script>

</head>
<body>

 <form name="form1" action="admin-save.php" method="post" onSubmit="return cek_form(this)" enctype="multipart/form-data" >
        <table>
            <tr>
                <td>User ID</td>
                <td><input type="text" name="user" class="txt"
                maxlength="5" ></td>
            </tr>	
            <tr>
                <td>Nama</td>
                <td><input type="text" name="nama" class="txt"
                maxlength="50" ></td>
            </tr>
             <tr>
                <td>Password</td>
                <td><input type="password" name="pass" class="txt"
                maxlength="6" ></td>
            </tr>
            <tr>
                <td>Re-Password</td>
                <td><input type="password" name="repass" class="txt"
                maxlength="6" ></td>
            </tr>
            <tr>
                <td>Foto</td>
                <td> <input name="foto" type="file"  /></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Simpan" class="btn">
                <input type="reset" value="Reset" class="btn"></td>
            </tr>
        </table>
    </form>
    
<h3><a href="admin-list.php">Kembali</a></h3>
</body>
</html>
