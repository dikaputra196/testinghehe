<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>PERPUSTAKAAN | DAFTAR BUKU</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<?php
//menggunakan fungsi dari file PHP lain
include "koneksi.php";

//query SQL join 4 table
$sSql="SELECT * FROM t_buku a 
	inner join t_judul b on a.id_judul = b.id_judul 
	inner join t_kategori c on b.id_kategori = c.id_kategori
	inner join t_user d on d.id_user = a.id_user
	where (a.status = 1)
	order by 1;";

//jika query error maka akan keluar dari semua fungsi
$result=mysqli_query($koneksi, $sSql) or die(mysqli_error());

//membuat nomor urut
$counter = 1; 

//menghitung jumlah record dari result set
echo "<h1>Jumlah Data: ". mysqli_num_rows($result)."</h1>";

if(mysqli_num_rows($result) > 0){ //jika query menghasilkan record
	//membentuk struktur tabel
	echo "<table border=\"1\"> 
			<tr>
				<th class=\"no\">No</th>
				<th class=\"no\">Kode Buku</th>
				<th>Judul Buku</th>
				<th>Kategori</th>
				<th>User</th>
				<th></th>
			</tr>";

	//mengisi record pada tabel
	while($data=mysqli_fetch_array($result))
	{
	echo "<tr>
			<td class=\"no\">$counter</td>
			<td>$data[id_buku]</td>
			<td>$data[judul]</td>
			<td>$data[kategori]</td>
			<td>$data[nama]</td>
			<td class='maintenance'> <a href=\"detail.php?id=$data[id_buku]\">detail</a></td>
		  </tr>";
		$counter++;
	} 
	//menutup struktur dari tabel
	echo "</table>";
}
else {
	//jka query tidak menghasilkan record
	echo "<h2>Data Buku Tidak Ditemukan</h2>";
	}
?>


</body>
</html>

