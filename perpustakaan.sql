-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2018 at 11:54 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `perpustakaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_buku`
--

CREATE TABLE IF NOT EXISTS `t_buku` (
  `id_buku` varchar(11) NOT NULL,
  `id_judul` int(4) NOT NULL DEFAULT '0',
  `id_user` varchar(5) NOT NULL,
  `tgl_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_buku`
--

INSERT INTO `t_buku` (`id_buku`, `id_judul`, `id_user`, `tgl_input`, `status`) VALUES
('20140420001', 1, 'agus', '2014-04-23 09:13:06', 1),
('20140420002', 22, 'agus', '2014-04-23 09:13:06', 1),
('20140420003', 23, 'agus', '2014-04-23 09:13:44', 1),
('20140420004', 2, 'admin', '2014-04-23 09:13:44', 1),
('20140420005', 15, 'agus', '2014-04-23 09:16:56', 1),
('20140420006', 2, 'admin', '2014-04-23 09:16:56', 1),
('20140420007', 4, 'agus', '2014-04-23 09:16:56', 1),
('20140420008', 17, 'admin', '2014-04-23 09:16:56', 1),
('20140420009', 24, 'agus', '2014-04-23 09:16:56', 1),
('20140420010', 7, 'agus', '2014-04-23 09:16:56', 1),
('20140420011', 18, 'agus', '2014-04-23 09:16:56', 1),
('20140420012', 8, 'agus', '2014-04-23 09:16:56', 0),
('20140420013', 11, 'agus', '2014-04-23 09:16:56', 1),
('20140420014', 12, 'agus', '2014-04-23 09:16:56', 1),
('20140420015', 20, 'agus', '2018-05-09 10:02:23', 1),
('20140420016', 21, 'agus', '2018-05-09 10:02:42', 1),
('20140420017', 25, 'agus', '2018-05-09 10:03:25', 1),
('20140420018', 6, 'agus', '2018-05-16 09:08:42', 1),
('20140420019', 7, 'agus', '2018-05-16 09:08:52', 1),
('20140420020', 9, 'agus', '2018-05-16 09:09:28', 1),
('20140420021', 12, 'agus', '2018-05-16 09:09:28', 1),
('20140420022', 11, 'agus', '2018-05-16 09:09:28', 1),
('20140420023', 9, 'agus', '2018-05-16 09:09:28', 1),
('20140420024', 2, 'agus', '2018-05-16 09:08:18', 1),
('20140420025', 4, 'agus', '2018-05-16 09:08:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_judul`
--

CREATE TABLE IF NOT EXISTS `t_judul` (
`id_judul` int(4) NOT NULL,
  `judul` varchar(100) NOT NULL DEFAULT '-',
  `id_kategori` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_judul`
--

INSERT INTO `t_judul` (`id_judul`, `judul`, `id_kategori`, `status`) VALUES
(1, '101 Tip dan Trik Pemrograman PHP', 1, 1),
(2, 'Algoritma & Pemrograman Dengan Borland C++', 1, 1),
(3, 'Algoritma & Pemrograman Menggunakan Java', 1, 1),
(4, 'Konsep Dasar Pemrograman Bahasa C', 1, 1),
(5, 'Pemrograman Internet Dan Xml Pada Ponsel Dengan Midlet Java', 1, 1),
(6, 'Pemrograman Visual Basic 6.0 Dan Microsoft Access', 1, 1),
(7, '25 Inspirasi Desain Wedding Invitation Dengan Coreldraw Floral,Retro,Couple,Unique & Heart', 3, 1),
(8, ' 	 3 In 1 Aplikasi Grafis Langsung Bisa Desain Grafis Tanpa Guru ', 3, 1),
(9, 'Adobe Dreamweaver Cs5 Untuk Beragam Desain Website Interaktif ', 3, 1),
(10, '42 Contoh Desain Corel Draw 12', 3, 1),
(11, 'Information Technology And Mobile Communication ', 4, 1),
(12, 'Handbook Of Algorithms For Wireless Networking And Mobile Computing', 4, 1),
(13, 'Membangun Aplikasi Mobile Dengan Qt Sdk', 4, 1),
(14, 'Mudah Membuat Mobile Application Dengan Flash Lite 3.0', 4, 1),
(15, 'Matematika Numerik Dengan Implementasi Matlab', 7, 1),
(16, 'Matematika Untuk Ilmu Fisika & Teknik', 7, 1),
(17, 'Logika Matematika Untuk Ilmu Komputer ', 7, 1),
(18, 'Matematika Dasar Untuk Perguruan Tinggi ', 7, 1),
(19, '15 Program Bantu Populer Untuk Mengembangkan & Mengelola Situs Web Anda', 5, 1),
(20, 'AJAX Membangun Web Dengan Teknologi ASYNCHRONOUSE JavaScript & XML ', 5, 1),
(21, ' 	 Aplikasi Web Database Dengan Dreamweaver Dan Php-Mysql ', 5, 1),
(22, 'Aplikasi Web Dengan Xml Menggunakan Dreamweaver 8', 5, 1),
(23, 'Pemrograman Web Database Menggunakan ADODB PHP', 5, 1),
(24, 'Seri Desain Web Promosi Web & Registrasi Domain (Dengan Berbagai Teknik Dan Tool)', 5, 1),
(25, 'Panduan Lengkap Menggunakan ZOPE Membangun dan Membuat Aplikasi Web', 5, 1),
(26, 'Panduan Lengkap Menguasai Pemrograman Web dengan PHP 5', 5, 1),
(27, 'Web Hacking Serangan Dan Pertahanannya', 5, 1),
(28, 'Visual Web Developer Untuk Pengembangan Aplikasi Web Dinamis', 5, 1),
(29, 'Cermat Berbahasa Indonesia Untuk Perguruan Tinggi', 6, 1),
(30, 'Kamus Peribahasa Bahasa Indonesia', 6, 1),
(31, 'Teori Bahasa Dan Otomata', 6, 1),
(32, 'Pedoman Umum Ejaan Bahasa Indonesia Yang Disempurnakan & Pedoman Umum Pembentukan Istilah', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori`
--

CREATE TABLE IF NOT EXISTS `t_kategori` (
`id_kategori` tinyint(1) NOT NULL,
  `kategori` varchar(100) NOT NULL DEFAULT '-',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kategori`
--

INSERT INTO `t_kategori` (`id_kategori`, `kategori`, `status`) VALUES
(1, 'Pemrograman', 1),
(2, 'Prosiding', 1),
(3, 'Desain', 1),
(4, 'Mobile', 1),
(5, 'Web', 1),
(6, 'Sastra', 1),
(7, 'Matematika', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `id_user` varchar(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(60) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_user`, `nama`, `password`, `email`, `hp`, `status`, `foto`) VALUES
('adi', 'Putu Adi', 'e10adc3949ba59abbe56e057f20f883e', 'putu.adi@yahoo.com', '08123456789', 0, 'Minion-Bananas-icon.png'),
('admin', 'Administrator', 'e10adc3949ba59abbe56e057f20f883e', 'perpustakaan@stikom-bali.ac.id', '0361244445', 1, 'Minion-Amazed-icon.png'),
('agus', 'Made Agus', 'e10adc3949ba59abbe56e057f20f883e', 'made.agus@yahoo.com', '08123456789', 1, 'Minion-Bananas-icon.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_buku`
--
ALTER TABLE `t_buku`
 ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `t_judul`
--
ALTER TABLE `t_judul`
 ADD PRIMARY KEY (`id_judul`), ADD UNIQUE KEY `judul` (`judul`);

--
-- Indexes for table `t_kategori`
--
ALTER TABLE `t_kategori`
 ADD PRIMARY KEY (`id_kategori`), ADD UNIQUE KEY `kategori` (`kategori`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
 ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_judul`
--
ALTER TABLE `t_judul`
MODIFY `id_judul` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `t_kategori`
--
ALTER TABLE `t_kategori`
MODIFY `id_kategori` tinyint(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
