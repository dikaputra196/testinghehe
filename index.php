<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>PERPUSTAKAAN | KONEKSI DATABASE</title>
<link href="style.css" type="text/css" rel="stylesheet" />

</head>
<body>
<?php
$servername="localhost";
$username="root";
$password="";
$database="perpustakaan";

$koneksi=mysqli_connect($servername, $username, $password);

if(!$koneksi){ //jika koneksi ke server gagal
	//keluar dari semua fungsi
	die("<h1>KONEKSI KE DATABASE SERVER GAGAL</h1>");
}
else {
	echo "<h1>KONEKSI KE DATABASE SERVER BERHASIL</h1>";
	
	if(mysqli_select_db($koneksi, $database))
	{
		echo "<h2>DATABASE '$database' DITEMUKAN</h2>";
	}
	else
	{
		//keluar dari semua fungsi
		die("<h2>DATABASE '$database' TIDAK DITEMUKAN<h2>");
	}
}
//echo "selamat";
echo "Masuk ke halaman <a href='index2.php'>home</a>";
?>

</body>
</html>
