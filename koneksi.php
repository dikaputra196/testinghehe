<?php
$servername="localhost";
$username="root";
$password="";
$database="perpustakaan";

$koneksi=mysqli_connect($servername, $username, $password);

//cek koneksi ke server
if(!$koneksi){	//koneksi gagal
	//keluar dari semua fungsi
	die("<h1>KONEKSI KE DATABASE SERVER GAGAL</h1>");
}
else { //koneksi berhasil
	//cek database
	if(!mysqli_select_db($koneksi, $database)) //database tidak ada di server
	{
		//keluar dari semua fungsi
		die("<h3>DATABASE TIDAK DITEMUKAN</h3>");
	}
}
?>